import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import AddVoiture from './voiture/AddVoiture';
import ListVoiture from './voiture/ListVoiture';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './Header';

function App() {
  return (
    <div className="container">
      <BrowserRouter>
      <Header/>
      <Routes>
        <Route path='/listVoiture' element={<ListVoiture/>}   />
        <Route path='/addVoiture' element={<AddVoiture/>}   />
      </Routes>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
