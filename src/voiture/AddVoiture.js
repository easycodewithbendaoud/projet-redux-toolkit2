import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addVoiture } from '../features/voiture/voitureSlice'
import { useNavigate } from 'react-router-dom'


function AddVoiture() {
    const [matricule,setMatricule]=useState("")
    const [marque,setMarque]=useState("")
    const [categorie,setCategorie]=useState("")
    const [reserve,setReserve]=useState(false)
    const navigate=useNavigate()
    const dispatch=useDispatch()
    function handleSubmit(ev){
        ev.preventDefault()
      const myVoiture={matricule:matricule,marque:marque,categorie:categorie,reserve:reserve}
      dispatch(addVoiture(myVoiture))
      navigate('/listVoiture')

    }
  return (
    <form onSubmit={(ev)=>handleSubmit(ev)}>
        <h2>Ajouter voiture</h2>
  <div className="mb-3">
    <label for="matricule" className="form-label">matricule</label>
    <input type="text" className="form-control" id="matricule" onChange={(ev)=>setMatricule(ev.target.value)} />
  </div>

  <div className="mb-3">
    <label for="marque" className="form-label">marque</label>
    <input type="text" className="form-control" id="marque"  onChange={(ev)=>setMarque(ev.target.value)}/>
  </div>

  <div className="mb-3">
    <label for="categorie" className="form-label">categorie</label>
    <input type="text" className="form-control" id="categorie" onChange={(ev)=>setCategorie(ev.target.value)}/>
  </div>
  <div className="mb-3">
    <label for="reserve" className="form-label">reserve</label>
    <input type="checkbox" className="form-checkbox" id="matricule" checked={reserve}  onChange={()=>setReserve(!reserve)}/>
  </div>
 
  <button type="submit" className="btn btn-primary">
    
  <i class="bi bi-plus-square"></i>
  Ajouter
    
    </button>
</form>
  )
}

export default AddVoiture