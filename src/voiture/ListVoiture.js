import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toggleReservation,addVoiture, deleteVoiture } from '../features/voiture/voitureSlice'
import { Link } from 'react-router-dom'

function ListVoiture() {
    const voitures=useSelector(state=>state.voitureData.voitures)
    const dispatch=useDispatch()
  return (
    <div>
        <h2> ListVoiture</h2>
        <Link to="/addVoiture">nouvelle voiture</Link>
  <table className='table table-stirped'>
<thead>
    <tr>
        <th>matricule</th>
        <th>marque</th>
        <th>categorie</th>
        <th>reservé</th>
        <th>supprimer</th>
    </tr>
</thead>
<tbody>
    {voitures.map(v=>{
        return(
            <tr key={v.id}>
                <td>{v.matricule}</td>
                <td>{v.marque}</td>
                <td>{v.categorie}</td>
                <td><input type="checkbox" checked={v.reserve} onChange={()=>dispatch(toggleReservation(v.id))} /></td>
                <td><button className='btn btn-danger' onClick={()=>dispatch(deleteVoiture(v.id))}>
                <i class="bi bi-x-circle-fill"></i>
                    supprimer</button></td>
            </tr>
        )
    })}
</tbody>
  </table>

       </div>
  )
}

export default ListVoiture