import { createSlice,nanoid } from "@reduxjs/toolkit";
const initState={voitures:[]}
const voitureSlice=createSlice({
    name:"voitures",
    initialState:initState,
    reducers:{
        addVoiture:{
            reducer:(state,action)=>{
                state.voitures.push(action.payload)
            },
            prepare:(voiture)=>{
                voiture.id=nanoid()
                voiture.dateCreation= new Date()
                return{payload:voiture}
            }
        },
        toggleReservation:(state,action)=>{
            const myVoiture=state.voitures.find(v=>v.id===action.payload)
            if(myVoiture){
                myVoiture.reserve=!myVoiture.reserve
            }
        },
        deleteVoiture:(state,action)=>{
            state.voitures=state.voitures.filter(v=>v.id!==action.payload)
        },
        updateVoiture:(state,action)=>{
            const index=state.voitures.findIndex(v=>v.id===action.payload.id)
        if(index>=0){
            state.voitures[index]=action.payload
        }
        
        }

    }
})

export const {addVoiture,toggleReservation,deleteVoiture}=voitureSlice.actions
const voitureReducer =voitureSlice.reducer
export default voitureReducer