import { createSlice } from "@reduxjs/toolkit";
const initState={users:[]}
const userSlice=createSlice({
    name:"users",
    initialState:initState,
    reducers:{}
})

const userReducer=userSlice.reducer
export default userReducer