import { configureStore } from "@reduxjs/toolkit";
import voitureReducer from "./voiture/voitureSlice";
import userReducer from "./user/userSlice";
export const store=configureStore({
    reducer:{voitureData:voitureReducer,
    userData:userReducer
    }
})